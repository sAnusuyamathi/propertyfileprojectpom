package pages;

import org.openqa.selenium.By;

import base.ProjectSpecificMethods;

public class NewLegalEntityPage extends ProjectSpecificMethods {

	public NewLegalEntityPage verifyPage() {
		if(driver.getTitle().contains("New Legal Entity")) {
			System.out.println("New Legal Entity is launched");
		}
		return this;
	}
	
	public NewLegalEntityPage enterName() {
		driver.findElement(By.xpath(prop.getProperty("NewLegalEntityPage.Name.xpath"))).sendKeys("BootCamp Anu");
		return this;
	}

	public NewLegalEntityPage enterDesc() {
		driver.findElement(By.xpath(prop.getProperty("NewLegalEntityPage.Description.xpath"))).sendKeys("TEst:");
		return this;
		
	}
	public LegalEntityPage clickSave() {
		driver.executeScript("arguments[0].click();",driver.findElement(By.xpath(prop.getProperty("NewLegalEntityPage.Save.xpath"))));
		return new LegalEntityPage();
	}
}
