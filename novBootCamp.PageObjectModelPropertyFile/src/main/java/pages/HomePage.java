package pages;

import org.openqa.selenium.By;

import base.ProjectSpecificMethods;

public class HomePage extends ProjectSpecificMethods{

	public HomePage verifyPage() throws InterruptedException {
//		System.out.println("Home page is launched");
		Thread.sleep(10000);
		if(driver.getTitle().contains("Home")) {
			System.out.println("Home page is launched");
		}
		return this;
	
	}
	
	public HomePage clickAppLauncher() {
		driver.executeScript("arguments[0].click();", driver.findElement(By.className(prop.getProperty("HomePage.applauncher.className"))));
		return this;
	}
	public HomePage clickViewAll() {
		driver.findElement(By.xpath(prop.getProperty("HomePage.ViewAll.xpath"))).click();
		return this;
	}
	
	public LegalEntityPage clickLegal() {
		driver.executeScript("arguments[0].click();",driver.findElement(By.xpath(prop.getProperty("HomePage.LegalEntity.xpath"))));
		return new LegalEntityPage();
	}

}
