package pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.ProjectSpecificMethods;

public class EditLegalEntityPage extends ProjectSpecificMethods {
	public WebDriverWait wait;
	public EditLegalEntityPage enterCompany() {
		driver.executeScript("arguments[0].value='Testleaf123';",driver.findElement(By.xpath(prop.getProperty("EditLegalEntityPage.Company.xpath"))));
		return this;
	}
	public EditLegalEntityPage enterDescription() {
		driver.findElement(By.xpath(prop.getProperty("EditLegalEntityPage.Description.xpath"))).sendKeys("SalesForce");
		return this;
	}
	
	public EditLegalEntityPage selectStatusActive() {
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath(prop.getProperty("EditLegalEntityPage.StatusDefault.xpath"))));
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath(prop.getProperty("EditLegalEntityPage.StatusActive.xpath"))));
		return this;
	}
	
	public LegalEntityPage clickSave() {
		wait=new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("EditLegalEntityPage.Save.xpath"))));
		driver.executeScript("arguments[0].click();",driver.findElement(By.xpath(prop.getProperty("EditLegalEntityPage.Save.xpath"))));
		return new LegalEntityPage();
	}
}
