package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.ProjectSpecificMethods;

public class DeleteLegalEntityPage extends ProjectSpecificMethods{

	
	public LegalEntityPage ClickDeleteLegal() {
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span[text()='Delete']")));
		return new LegalEntityPage();
	}


	}
	

