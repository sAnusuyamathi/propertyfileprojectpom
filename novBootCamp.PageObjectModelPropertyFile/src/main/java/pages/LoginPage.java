package pages;

import org.openqa.selenium.By;

import base.ProjectSpecificMethods;

public class LoginPage extends ProjectSpecificMethods {
		
	public LoginPage enterUserName() {
		driver.findElement(By.id(prop.getProperty("LoginPage.username.id"))).sendKeys(prop.getProperty("username"));
		return this;
	}
	public LoginPage enterPassWord() {
		driver.findElement(By.id(prop.getProperty("LoginPage.password.id"))).sendKeys(prop.getProperty("password"));
		return this;
	}
	
	public HomePage clickLogin() {
		driver.findElement(By.id(prop.getProperty("LoginPage.Login.id"))).click();
		return new HomePage();
	}
}
