package pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.ProjectSpecificMethods;

public class LegalEntityPage extends ProjectSpecificMethods {
	WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(5));
	public LegalEntityPage verifyLegalEntityPage() {
		
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.Tab.xpath")))));
		if(driver.getTitle().contains("Legal Entities")) {
			System.out.println("Legal Entity page is launched");
			
		}
		
		return this;
	}
	public LegalEntityPage clickLegalEntityTab() {
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.ClickLegalTab.xpath"))));
//		5. Click on New Legal Entity
			
		return this;

	}
	
	public NewLegalEntityPage clickNewLegalEntity() {
	driver.executeScript("arguments[0].click();",driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.ClickNewLegalEntity.xpath"))));
	
	return new NewLegalEntityPage();
	}
	public LegalEntityPage verifyCreationToastMessage() {
		
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.ToastMsg.xpath")))));
		
		if(driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.ToastMsg.xpath"))).getText().contains("Legal Entity \"BootCamp Anu\" was created. ")){
		System.out.println("Legal entity is created");
	}
		return this;
	}
	
	public LegalEntityPage searchLegalEntity() {
		driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.SearchText.xpath"))).sendKeys("BootCamp Anu");
		driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.SearchText.xpath"))).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.SearchText.xpath"))).sendKeys(Keys.TAB);
		return this;

	}
	
	public LegalEntityPage verifyLegalEntityTable() {
		if(driver.findElements(By.xpath(prop.getProperty("LegalEntityPage.SearchResultGrid.xpath"))).size() > 0){
			System.out.println("Results found");
		}
		else {
			System.out.println("No results found");
		}
		return this;

	}
	
	public LegalEntityPage clickShowActions() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("LegalEntityPage.ShowActions.xpath"))));
//		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath("//tbody//tr[1]/td[5]//following::span[text()='Show more actions']")));
		WebElement actionsMenu = driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.ShowActions1stRow.xpath")));
		Actions builder = new Actions(driver);
		builder.moveToElement(actionsMenu).click().perform();
		return this;
	}
	
	
	public EditLegalEntityPage clickEditFromActionsMenu() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("LegalEntityPage.EditFromShowActions.xpath"))));
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.EditFromShowActions.xpath"))));
		return new EditLegalEntityPage();
	}
	
	public DeleteLegalEntityPage clickDeleteFromActionsMenu() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("LegalEntityPage.DeleteFromShowActions.xpath"))));
		driver.executeScript("arguments[0].click();", driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.DeleteFromShowActions.xpath"))));
		return new DeleteLegalEntityPage();
	}
	
	public void verifySaveToastMessage() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.ToastMsg.xpath")))));
		if(driver.findElement(By.xpath(prop.getProperty("LegalEntityPage.ToastMsg.xpath"))).getText().contains("Legal Entity \"BootCamp Anu\" was saved. ")){
			System.out.println("Legal entity is edited");
		}

	}
	
	public void verifyDeletion() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@data-aura-class='forceActionsText']")));
		if(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText().contains("was deleted"))
		{
		System.out.println("Legal Entity is deleted");
		}	
	}
	
}
