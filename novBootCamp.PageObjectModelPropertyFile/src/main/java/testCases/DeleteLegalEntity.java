package testCases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.ProjectSpecificMethods;
import pages.LoginPage;

public class DeleteLegalEntity extends ProjectSpecificMethods {
//	@Parameters({"uName","pWord"})
	@Test
	public void runCreateLegalEntity() throws InterruptedException {
		new LoginPage()
		.enterUserName()
		.enterPassWord()
		.clickLogin()
		.verifyPage()
		.clickAppLauncher()
		.clickViewAll()
		.clickLegal()
		.verifyLegalEntityPage()
		.searchLegalEntity()
		.verifyLegalEntityTable()
		.clickShowActions()
		.clickDeleteFromActionsMenu()
		.ClickDeleteLegal()
		.verifyDeletion();
		
	}
}
