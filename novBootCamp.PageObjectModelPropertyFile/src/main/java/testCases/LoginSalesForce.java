package testCases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.ProjectSpecificMethods;
import pages.LoginPage;

public class LoginSalesForce extends ProjectSpecificMethods{
//	@Parameters({"uName","pWord"})
	@Test
	public void runLogin() throws InterruptedException {
		new LoginPage()
		.enterUserName()
		.enterPassWord()
		.clickLogin()
		.verifyPage();

	}
	
}
